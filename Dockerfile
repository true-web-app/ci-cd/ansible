FROM cytopia/ansible:2.9-tools

# install galaxy plugins
RUN ansible-galaxy collection install community.general
RUN ansible-galaxy collection install community.docker

# install python plugins
COPY requirements.txt .
RUN python3 -m pip install -r requirements.txt
