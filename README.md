
# True Web Ansible

Built on [cytopia/ansible:latest-tools](https://hub.docker.com/r/cytopia/ansible)

Added:  
- collection  `community.general` from `ansible-galaxy`
- collection `community.docker` from `ansible-galaxy` 
- pymongo support (for mongo dynamic inventory)
